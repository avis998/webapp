# Instrukcja
### Pobrany klucz przerzucamy do odpowiedniego katalogu
```cp ./ssh.pem ~/.ssh/```
### Ustawiamy odpowiednie uprawnienia do pliku
```chmod 600 ~/.ssh/ssh.pem```
### Teraz konieczne jest podłączenie się do serwera
```ssh-add ~/.ssh/ssh.pem``` </br>
```ssh ubuntu@<adres_ip_serwera>```
### Konieczna jest instalacja i upewnienie się, że są zainstalowane paczki
```node -v```</br>
```pm2 -v```</br>
```nginx -v```
### Jeżeli którejś z nich brakuje, można je zainstalować tymi komendami
```curl -sL https://deb.nodesource.com/setup_16.x | sudo bash -```</br>
```sudo apt-get update```</br>
```sudo apt-get install -y nodejs nginx```</br>
```sudo npm install -g pm2```
### Właczamy nginx i sprawdzamy jego status (Na maszynie jest już skonfigurowany)
```sudo systemctl restart nginx```</br>
```sudo systemctl status nginx```
### Wchodzimy do folderu z frontendem i uruchamiamy go instalując wcześniej paczki
```cd /var/www/webapp/front```</br>
```npm ci```</br>
```npm run build```
### To samo robimy z backendem
```cd /var/www/webapp```</br>
```npm ci```</br>
```pm2 start index.js```
### Aplikacja będzie otwarta pod adresem IP serwera