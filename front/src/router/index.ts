import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import StartView from '../views/StartView.vue'
import RingingView from '../views/RingingView.vue'
import Connected from '../views/ConnectedView.vue'
import Failed from '../views/FailedView.vue'
import Answered from '../views/AnsweredView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'start',
    component: StartView
  },
  {
    path: '/',
    name: 'ringing',
    component: RingingView
  },
  {
    path: '/',
    name: 'failed',
    component: Failed
  },
  {
    path: '/',
    name: 'connected',
    component: Connected
  },
  {
    path: '/',
    name: 'answered',
    component: Answered
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
